//
//  ViewController.swift
//  L3_workwithnumbers_delete
//
//  Created by Anatolii on 3/23/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let TestNumber1 = 256, TestNumber2 = 15
        
        //Вывести на экран наибольшее из двух чисел
        TwoNumbersCompare(TestNumber1: TestNumber1,TestNumber2: TestNumber2)
        //Вывести на экран квадрат и куб числ
        SquareNumber(TestNumber1: TestNumber1)
        //Вывести на экран все числа до заданного и в обратном порядке до 0
        ShowNumbers(TestNumber1: TestNumber1)
        //Проверить, является ли заданное число совершенным и посчитать делители
        Deliteli(TestNumber1: TestNumber1)
        
        //Задача 1  Остров Манхэттен был приобретен поселенцами за $24 в 1826 г. Каково
        //было бы в настоящее время состояние их счета, если бы эти 24 доллара были
        //помещены тогда в банк под 6% годового дохода?
        
        //вводные аргументы  для задачи с Манхеттеном
        let price=24
        let yearPercent=6
        let year=2017
        
        //Задача 2 Ежемесячная стипендия студента составляет А гривен, а расходы на проживание превышают ее и составляют B грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3% Определить, какую нужно иметь сумму денег, чтобы прожить учебный год (10 месяцев), используя только эти деньги и стипендию.
        
        //Задача 3  У студента имеются накопления S грн. Ежемесячная стипендия составляет А гривен, а расходы на проживание превышают ее и составляют B грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3% Определить, сколько месяцев сможет прожить студент, используя только накопления и стипендию.
        
        
        //вводные аргументы для задачи 2,
        //а так же для задачи 3
        let stipendiya=100
        let tratu=200
        let procentPovusheniyaTrat=3
        let summaNakopleniy=1000
        
        
        //Задача 4 вводите любую трехзначную  цело численную переменную типа 256, 341, 712 После выполнения вашей программы у вас в другой переменной должно лежать это же число только задом на перед 652, 143, 217
        
        //аргумент для задания получить число задом наперед
        let chislo=398
        
        manhattan(price: price, yearPercent: yearPercent, year:year)
        skolkoDenegStudentyNaGod(stipendiya: stipendiya,
                                 tratu: tratu,
                                 procentPovusheniyaTrat: procentPovusheniyaTrat)
        skolkoProzhuvetStudent(stipendiya: stipendiya,
                               tratu: tratu,
                               procentPovusheniyaTrat: procentPovusheniyaTrat,
                               summaNakopleniy: summaNakopleniy)
        chisloZadomNapered(chislo: chislo)
    }
    
    func TwoNumbersCompare(TestNumber1: Int,TestNumber2: Int) {
        print("Number1: \(TestNumber1); Number2:  \(TestNumber2)")
        if TestNumber1 == TestNumber2 {
            print("\(TestNumber1) = \(TestNumber2)")
        }
        else {
            print("\(TestNumber1) \(TestNumber1<TestNumber2 ? "<" : ">") \(TestNumber2)")
        }
    }
    
    func SquareNumber(TestNumber1: Int) {
        print("Number entered: \(TestNumber1)")
        print("Square number: \(TestNumber1*TestNumber1)")
        print("Triple  number: \(TestNumber1*TestNumber1*TestNumber1)")
    }
    
    func ShowNumbers(TestNumber1: Int) {
        var count1 = 0, count2 = TestNumber1
        for _ in 0...TestNumber1 {
            print("\(count1)  \(count2)")
            count1+=1
            count2-=1
        }
    }
    
    func Deliteli(TestNumber1: Int) {
        var count = 1, chisloDeliteley = 0, PerfectNumber = 0
        print("Test number: \(TestNumber1)")
        for _ in 0..<TestNumber1 {
            if (TestNumber1 % count)==0 {
                print("\(count)")
                chisloDeliteley+=1
                if count != TestNumber1 {
                    PerfectNumber+=count
                }
            }
            count+=1
        }
        print("Kolichestvo  deliteley: \(chisloDeliteley)")
        if PerfectNumber==TestNumber1 {
            print("Number \(TestNumber1) is perfect")
        }
        else {
            print("Number \(TestNumber1) is NOT perfect")
        }
    }
    
    func manhattan(price:Int, yearPercent:Int, year:Int) {
        var income=Double(price)
        for _ in year..<2019 {
            income+=income*(Double(yearPercent)/100)
        }
        print("price for Manhattan buying: \(price)")
        print("year of Manhattan buying: \(year)")
        print("year percent of bank deposit: \(yearPercent)")
        print("total amount of bank deposit in 2019 would be: \(income)")
    }
    
    func skolkoDenegStudentyNaGod(stipendiya: Int,
                                  tratu: Int,
                                  procentPovusheniyaTrat: Int) {
        var tratuZaGod=Double(tratu)
        var tratuVMesyac=Double(tratu)
        let kolvoMesyacev=10
        for _ in 1..<kolvoMesyacev {
            tratuVMesyac+=tratuVMesyac*Double(procentPovusheniyaTrat)/100
            tratuZaGod+=tratuVMesyac
        }
        tratuZaGod-=Double(stipendiya*kolvoMesyacev)
        print("Summa stipendii: \(stipendiya)")
        print("Summa trat v mesyac: \(tratu)")
        print("Procent povusheniya trat: \(procentPovusheniyaTrat)")
        print("Summa neobhodimaya studentu na 10 mesyacev minus stipendiya: \(tratuZaGod)")
    }
    
    func skolkoProzhuvetStudent(stipendiya: Int,
                                tratu: Int,
                                procentPovusheniyaTrat: Int,
                                summaNakopleniy: Int) {
        var kolvoMesyacev=0
        var TratuKazhduyMesyac=Double(tratu)
        var SummaUStudenta=Double(summaNakopleniy+stipendiya-tratu)
        while SummaUStudenta>0 {
            TratuKazhduyMesyac+=TratuKazhduyMesyac*Double(procentPovusheniyaTrat)/100
            SummaUStudenta=SummaUStudenta+Double(stipendiya)-TratuKazhduyMesyac
            kolvoMesyacev+=1
        }
        print("Summa stipendii: \(stipendiya)")
        print("Summa trat v mesyac: \(tratu)")
        print("Procent povusheniya trat: \(procentPovusheniyaTrat)")
        print("Summa nakopleniy: \(summaNakopleniy)")
        print("Kolvo mesyacev prozhuvet student: \(kolvoMesyacev)")
    }
    
    func chisloZadomNapered(chislo: Int) {
        
        var pervayaCufra=0
        var vtorayaCufra=0
        var tretyaCufra=0
        var razryad=100
        var DvePervuhCufru=0
        
        for _ in 0..<3 {
            switch razryad {
            case 10:
                DvePervuhCufru=Int(Double(chislo)/Double(razryad))
                vtorayaCufra=DvePervuhCufru-pervayaCufra*razryad
            case 100:
                pervayaCufra=Int(Double(chislo)/Double(razryad))
            default:
                tretyaCufra=chislo-DvePervuhCufru*10
            }
            razryad/=10
        }
        print("Chislo vveli: \(chislo)")
        print("Chislo naoborot: \(tretyaCufra*100+vtorayaCufra*10+pervayaCufra)")
    }
}

